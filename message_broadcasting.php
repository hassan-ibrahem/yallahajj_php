<?php
include('header.php');

# Includes the autoloader for libraries installed with composer
require __DIR__ . '/vendor/autoload.php';

# Imports the Google Cloud client library
use Google\Cloud\Translate\TranslateClient;
use Google\Cloud\Firestore\FirestoreClient;

$translate = new TranslateClient();


if(isset($_POST["btnSubmit"]))
{
	putenv("GOOGLE_APPLICATION_CREDENTIALS=C:\YallaHaj-a1f0948e11b7.json");

	$firestore = new FirestoreClient();
	$date = date('Y-m-d H:i:s A');
	
	if(isset($_POST["hiddenOriginalMessage"]))
	{
		$docRef = $firestore->collection('Messages')->add([
			'body' => $_POST['hiddenOriginalMessage'],
			'date' => $date,
			'type' => $_POST['messageCategory']
		]);
	}
	
	if(isset($_POST["englishTranslation"]))
	{
		$docRef = $firestore->collection('Messages_EN')->add([
			'body' => $_POST['englishTranslation'],
			'date' => $date,
			'type' => $_POST['messageCategory']
		]);

	}
	
	if(isset($_POST["arabicTranslation"]))
	{
		$docRef = $firestore->collection('Messages_AR')->add([
			'body' => $_POST['arabicTranslation'],
			'date' =>  $date,
			'type' => $_POST['messageCategory']
		]);

	}
	
	if(isset($_POST["frenchTranslation"]))
	{
		$docRef = $firestore->collection('Messages_FR')->add([
			'body' => $_POST['frenchTranslation'],
			'date1' =>  $date,
			'type' => $_POST['messageCategory']
		]);

	}
	$successResponse = true;
}
?>

<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Home</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						<li class="active">BroadCasting</li>
					</ul><!--.breadcrumb-->

</div>

<div class="page-content">
						
					<div class="page-header position-relative">
						<h1>
							Spread your message
							<small>
								<i class="icon-double-angle-right"></i>
								write your text and it will be translated automatically
							</small>
						</h1>
					</div><!--/.page-header-->
<?php

if(isset($successResponse))
{
?>
<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="icon-remove"></i>
								</button>

								<i class="icon-ok green"></i>

								All messages  queued successfully and will be forwarded.
								
</div>
<?php } ?>	
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->

<form class="form-horizontal" method="post" id="inputForm">

								<div class="control-group" style="width:70%">
										<textarea id="originalText" name="originalText" placeholder="Write your message here" style="width:100%" rows="4" cols="50"></textarea>
										
								</div>


								<h3 class="header smaller lighter blue">
									Select languages
									<small>Select target langugaes to start broadcasting</small>
								</h3>

								<div class="row-fluid">
									<div class="span5">
										<div class="control-group">

											<div class="controls">
												<label>
													<input type="checkbox" id="arCheckBox" name="arCheckBox"/>
													<span class="lbl">Arabic</span>
												</label>

												<label>
													<input type="checkbox" id="enCheckBox" name="enCheckBox" />
													<span class="lbl">English</span>
												</label>

												<label>
													<input type="checkbox" id="frCheckBox" name="frCheckBox" />
													<span class="lbl">French</span>
												</label>
											</div>
										</div>
											
									</div>
								</div>	

							
							
								<div class="form-actions">
									<button class="btn btn-info" type="submit">
										<i class="icon-exchange bigger-110"></i>
										Translate
									</button>

									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
										<i class="icon-undo bigger-110"></i>
										Reset
									</button>
								</div>

								<div class="hr"></div>
</form>
				

<form class="form-horizontal" method="post" id="broadcastForm">
				
							<?php 
							
							if(isset($_POST['originalText']) && !empty($_POST['originalText']))
							{
								$originalText = $_POST['originalText'];
		
							?>
	<input type="hidden" id="hiddenOriginalMessage" name="hiddenOriginalMessage" value="<?php echo $originalText;?>" />

								<div class="row-fluid">
								
								<div  script="float:right">
														<label for="messageCategory">Message category</label>

														<select id="messageCategory" name="messageCategory">
															<option value="INFO">Info</option>
															<option value="WARN">Warning</option>
															<option value="ADVICE">Advice</option>
															<option value="URGENT">Urgent</option>
														</select>
														
														<button id="btnSubmit" name="btnSubmit" class="btn btn-info" type="submit">
															<i class="icon-bullhorn bigger-110"></i>
															Broadcast
														</button>
										</div>
									<div class="span9">
										<div class="widget-box">
											<div class="widget-header">
												<h4>Messages</h4>

												<span class="widget-toolbar">
													<a href="#" data-action="collapse">
														<i class="icon-chevron-up"></i>
													</a>

													<a href="#" data-action="close">
														<i class="icon-remove"></i>
													</a>
												</span>
											</div>

											<div class="widget-body">
												<div class="widget-main">
												<?php
												
												if(isset($_POST['arCheckBox']) && !empty($_POST['arCheckBox']))
												{
													$result = $translate->translate($originalText, [
														'target' => 'ar',
													]);
												
												?>
													<div class="row-fluid">
														<label for="form-field-8">Arabic</label>

														<textarea class="span12" id="arabicTranslation"  name="arabicTranslation" placeholder="Default Text"><?php print("$result[text]"); ?></textarea>
													</div>

												
													<hr />
												<?php }	?>

												<?php
												
												if(isset($_POST['enCheckBox']) && !empty($_POST['enCheckBox']))
												{
													$result = $translate->translate($originalText, [
														'target' => 'en',
													]);
												
												?>
												<div class="row-fluid">
														<label for="form-field-9">English</label>

														<textarea class="span12" id="englishTranslation" name="englishTranslation" placeholder="Default Text"><?php print("$result[text]"); ?></textarea>
												</div>
												<?php }	?>
												
												<hr />
												<?php
												
												if(isset($_POST['frCheckBox']) && !empty($_POST['frCheckBox']))
												{
													$result = $translate->translate($originalText, [
														'target' => 'fr',
													]);
												
												?>
												<div class="row-fluid">
													<label for="form-field-11">French</label>

													<textarea class="span12" id="frenchTranslation" name="frenchTranslation" placeholder="Default Text"><?php print("$result[text]"); ?></textarea>
												</div>
												<?php }	?>
												</div>
											</div>
										</div>
									</div><!--/span-->
								</div>
								
								<div class="hr"></div>
								
								<?php
								
									}
								?>
</form>
						</div><!--/.span-->
					</div><!--/.row-fluid-->
					
										
				</div><!--/.page-content-->
				
<?php
include('footer.php');
?>
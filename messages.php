<?php
include('header.php');

# Includes the autoloader for libraries installed with composer
require __DIR__ . '/vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;


?>

<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="#">Home</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						<li class="active">Messages</li>
					</ul><!--.breadcrumb-->

</div>

<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							All Messages
							<small>
								<i class="icon-double-angle-right"></i>
								Here you will get all messages
							</small>
						</h1>
					</div><!--/.page-header-->

					<div class="row-fluid">
						<div class="span12">
						<div class="table-responsive">
			   <table id="example" class="table table-striped table-bordered table-hover">
					<thead>
						<tr> 
							<th>Body</th>
							<th>Type</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						<?php

							putenv("GOOGLE_APPLICATION_CREDENTIALS=C:\YallaHaj-a1f0948e11b7.json");
							
							$firestore = new FirestoreClient();
							$collectionReference = $firestore->collection('Messages');
							$snapshot = $collectionReference->documents();
								
							foreach ($snapshot as $value) {
							?>
								<tr> 
									<td><?php echo $value->get('body'); ?></td >
									<td><span class="<?php if ($value->get('type') == 'INFO') echo 'label label-info arrowed arrowed-righ'; else if ($value->get('type') == 'ADVICE') echo 'label label-success'; else if ($value->get('type') == 'WARN') echo 'label label-warning'; else if ($value->get('type') == 'URGENT') echo 'label label-inverse arrowed-in'; ?>"><?php echo $value->get('type'); ?></span></td >
									<td><?php echo $value->get('date'); ?></td >
								</tr>
							<?php
							
						}
						?>
					</tbody>
			   </table>
		</div>
						</div><!--/.span-->
					</div><!--/.row-fluid-->
					
										
				</div><!--/.page-content-->
				
<?php
include('footer.php');
?>